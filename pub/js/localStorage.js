define(function () {
    var keyValue = "flower-data",
        _valueIndex = "value",
        _titleIndex = "title",

        _getCurrentValue = function () {
            return JSON.parse(localStorage.getItem(keyValue))[_getCurrentIntDate()];
        },
        _getCurrentIntDate = function () {
            var date = new Date(),
                month = date.getMonth() + 1;
            return date.getFullYear() + ( (month > 10) ? "" : "0" ) + month + date.getDate();
        },
        _defaultOneItem = function () {
            var defaultData = {};
            defaultData[_valueIndex] = 0;
            defaultData[_titleIndex] = "Title";
            return defaultData;
        },
        _get = function (index) {
            return _getCurrentValue()[index];
        },
        _set = function (index, value) {
            var data = JSON.parse(localStorage.getItem(keyValue)),
                dataIndex = _getCurrentIntDate();
            if (typeof  data[dataIndex] === "undefined") {
                data[dataIndex] = _defaultOneItem();
            }
            data[dataIndex][index] = value;
            localStorage.setItem(keyValue, JSON.stringify(data));
        };

    (function () {
        try {
            if (_getCurrentValue() == null) {
                throw "..init..";
            }
        } catch (e) {
            (function () {
                var data = {};
                data[_getCurrentIntDate()] = _defaultOneItem();
                localStorage.setItem(keyValue, JSON.stringify(data));
            })();
        }
    })();

    return {
        getCounterValue: function () {
            return _get(_valueIndex);
        },
        getTitleValue: function () {
            return _get(_titleIndex);
        },
        setCounterValue: function (value) {
            _set(_valueIndex, parseInt(value, 10));
        },
        setTitleValue: function (value) {
            _set(_titleIndex, value);
        }
    }
});

requirejs.config({
    baseUrl: "js/",
    paths: {
        dollardom: 'dollardom.min'
    },
    shim: {
        'dollardom': {
            exports: '$dom'
        }
    }
});

requirejs(["material", "dollardom", "localStorage"], function (r, t, loc) {

    var button = $dom.get("#plus")[0],
        _setDomValue = function (element, value) {
            element.innerText = value;
        },
        title = $dom.get("#title")[0];

    title.value = loc.getTitleValue();

    title.addEventListener("change", function () {
        title.value = this.value;
        loc.setTitleValue(this.value);
    });

    button.innerText = loc.getCounterValue();
    button.addEventListener("click", function () {
        var val = parseInt(this.innerText, 10);
        _setDomValue(button, ++val);
        loc.setCounterValue(val);
    });

    $dom.get("#clear")[0].addEventListener("click", function () {
        loc.setCounterValue(0);
        _setDomValue(button, 0);
    })
});
